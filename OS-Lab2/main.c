#include <stdio.h>
#include <stdlib.h>

#include <windows.h>

#define SLEEP_TIME_MIN_MAX 100
#define SLEEP_TIME_AVERAGE 50

struct FindMinMaxData {
	int count;
	int* array;
	int result_min;
	int result_max;
};

struct FindAverageData {
	int count;
	int* array;
	double result;
};

DWORD WINAPI find_min_max(LPVOID lpParam) {
	struct FindMinMaxData* data = (struct FindMinMaxData*)lpParam;
	data->result_min = INT_MAX;
	data->result_max = INT_MIN;

	for (int i = 0; i < data->count; ++i) {
		if (data->array[i] < data->result_min)
			data->result_min = data->array[i];
		if (data->array[i] > data->result_max)
			data->result_max = data->array[i];
		Sleep(SLEEP_TIME_MIN_MAX);
	}

	printf("%s\n", "min max stopped");
	return 0;
}

DWORD WINAPI find_average(LPVOID lpParam) {
	struct FindAverageData* data = (struct FindAverageData*)lpParam;

	int sum = 0;
	for (int i = 0; i < data->count; ++i) {
		sum += data->array[i];
		Sleep(SLEEP_TIME_AVERAGE);
	}
	data->result = (double)sum / data->count;

	printf("%s\n", "average stopped");
	
	return 0;
}

int main() {
	printf("enter size and elements of the array\n");
	int array_size;
	scanf_s("%d", &array_size);

	int* numbers = malloc(sizeof(int) * array_size);
	if (numbers == NULL) {
		fprintf(stderr, "Couldn't allocate array");
		ExitProcess(1);
	}

	for (int i = 0; i < array_size; ++i) {
		scanf_s("%d", &numbers[i]);
	}

	struct FindMinMaxData min_max_data;
	min_max_data.count = array_size;
	min_max_data.array = numbers;
	struct FindAverageData average_data;
	average_data.count = array_size;
	average_data.array = numbers;

	HANDLE threads[2];
	DWORD min_max_thread_id, average_thread_id;

	threads[0] = CreateThread(NULL, 0, find_min_max, &min_max_data, 0, &min_max_thread_id);
	if (threads[0] == NULL) {
		fprintf(stderr, "Error starting %s thread\n", "min_max");
		ExitProcess(3);
	}
	threads[1] = CreateThread(NULL, 0, find_average, &average_data, 0, &average_thread_id);
	if (threads[1] == NULL) {
		fprintf(stderr, "Error starting %s thread\n", "average");
		ExitProcess(4);
	}

	printf("thread ids:\nmin_max: %lu\naverage: %lu\n", min_max_thread_id, average_thread_id);
	
	WaitForMultipleObjects(2, threads, TRUE, INFINITE);

	printf("results:\nmin: %d\nmax: %d\naverage: %lf\n", min_max_data.result_min, min_max_data.result_max, average_data.result);
}
